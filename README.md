# Nativescript BOTDesign job interview exercice

## Run a test on your phone

Follow this guide : https://nativescript.org/blog/nativescript-preview/

**Steps** :

- 1. ```npm install -g nativescript```
- 3. NativeScript Preview Install
    - [App Store (iOS)](https://itunes.apple.com/us/app/nativescript-playground/id1263543946)
    - [Google Play (Android)](https://play.google.com/store/apps/details?id=org.nativescript.play)
- 2. NativeScript Preview Install
    - [App Store (iOS)](https://itunes.apple.com/us/app/nativescript-preview/id1264484702)
    - [Google Play (Android)](https://play.google.com/store/apps/details?id=org.nativescript.preview)
- 3. ```git clone https://gitlab.com/etiennepericat/botdesign```
- 4. ```cd botdesign```
- 5. ```tns preview```
- 6. Scan the QR code, you're done !

## Run on your computer

Follow the officiel getting started page : https://docs.nativescript.org/angular/start/introduction