import { PokemonDetailsComponent } from './components/pokemon-details/pokemon-details.component';
import { NativeScriptFormsModule} from 'nativescript-angular';
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { PokemonItemComponent } from './components/pokemons/pokemon-item/pokemon-item.component';
import { PokemonsComponent } from './components/pokemons/pokemons.component';
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        ReactiveFormsModule,
        NativeScriptFormsModule,
    ],
    declarations: [
        AppComponent,
        PokemonsComponent,
        PokemonItemComponent,
        PokemonDetailsComponent
    ],
    providers: [],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
