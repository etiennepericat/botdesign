export interface Pokemon {
    id: string;
    name: string;
    img: string;
    type: string[];
}
