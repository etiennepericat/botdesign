import file from "../assets/pokemons.json";
import { Injectable } from "@angular/core";
import { Pokemon as Pokemon } from "../models/pokemon";

@Injectable({
    providedIn: "root"
})
export class PokemonService {

    /**
     * Returns the complete list of pokemons. 
     * (The given mock file)
     */
    getPokemons(): Pokemon[] {
        return file;
    }

    /**
     * Retrieve a specific pokemon or null if not found.
     * @param id the pokemon identifier
     */
    getPokemon(id: string): Pokemon {
        return file.filter((item: Pokemon) => item.id === id)[0] as Pokemon;
    }

    /**
     * Extract a list of types from the pokemons file
     */
    getTypes(): string[] {
        // flatten the types
        var flatArray: string[] = file.flatMap(poke => poke.type);

        // remove duplicates
        return [...new Set(flatArray)];
    }
}

