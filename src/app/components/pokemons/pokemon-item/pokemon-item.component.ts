import { Pokemon } from './../../../models/pokemon';
import { Component, OnInit, Input } from "@angular/core";
import { RouterExtensions } from 'nativescript-angular';


@Component({
    selector: "pokemon-item",
    templateUrl: "./pokemon-item.component.html",
    styleUrls: ['./pokemon-item.css']
})
export class PokemonItemComponent implements OnInit {

    @Input() item: Pokemon;

    constructor(private _router: RouterExtensions) { }

    ngOnInit(): void {
    }

    onItemTap(event) {
        console.log('grid tapped');
        this._router.navigate(['pokemons',this.item.id]);
    }
}
