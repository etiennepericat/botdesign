import { Component, OnInit } from "@angular/core";

import { Pokemon } from "../../models/pokemon";
import { PokemonService } from "../../services/pokemon.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
@Component({
    selector: "pokemons",
    templateUrl: "./pokemons.component.html",
    styleUrls: ["./pokemons.css"]
})
export class PokemonsComponent implements OnInit {

    // list variables
    pokemons: Array<Pokemon>;
    pokemonsBaseList: Array<Pokemon>;

    // input variables
    types: string[];

    // form
    pokeForm: FormGroup;

    constructor(private _pokemonService: PokemonService,
        private _fb: FormBuilder) { }

    ngOnInit(): void {

        // retrieve the list of pokemons (from the file)
        this.pokemons = this._pokemonService.getPokemons();
        this.pokemonsBaseList = this._pokemonService.getPokemons();

        // list of types
        this.types = this._pokemonService.getTypes();
        // adds an empty choice
        this.types.unshift('');

        // form initialization (reactiv form)
        this.pokeForm = this._fb.group({
            nameTextField: [''],
            numberTextField: [''],
            typeTextField: [],

        });

        // subscribe to name input changes (and call the filtering method)
        this.pokeForm.controls.nameTextField.valueChanges.subscribe(
            () => this.filterResults()
        )

        // subscribe to number input changes (and call the filtering method)
        this.pokeForm.controls.numberTextField.valueChanges.subscribe(
            () => this.filterResults()
        )

        // subscribe to type input changes (and call the filtering method)
        this.pokeForm.controls.typeTextField.valueChanges.subscribe(
            () => this.filterResults()
        )

    }

    /**
     * Filter the list of pokemons according to the 
     * user inputs. 
     * @param name the provided pokemon name (contains search)
     * @param number the provided pokemon id
     */
    filterResults() {
        var name = this.pokeForm.controls.nameTextField.value;
        var number = this.pokeForm.controls.numberTextField.value;
        var typeIdx = this.pokeForm.controls.typeTextField.value;

        var currentFilter = JSON.parse(JSON.stringify(this.pokemonsBaseList));
        if (name != null) {
            currentFilter = currentFilter.filter(poke => poke.name.toLowerCase().includes(name.toLowerCase()));
        }

        if (number) {
            currentFilter = currentFilter.filter(poke => Number(poke.id) === Number(number));
        }

        if (typeIdx) {
            currentFilter = currentFilter.filter(poke => poke.type.includes(this.types[typeIdx]));
        }

        this.pokemons = currentFilter;
    }

}
