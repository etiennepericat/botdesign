import { PokemonService } from '../../app/services/pokemon.service';
// A sample Jasmine test
describe("PokemonService UT", function() {
  it("getPokemons", function() {

    const pokemonService: PokemonService = new PokemonService();
    let size = pokemonService.getPokemons().length;
    expect(size).toBe(151);
  });
});
